
/**
 * KnexJs migration config
 */
module.exports = {
  client: 'mysql',
  connection: {
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB
  },
  migrations: {
    // the directory your migration files are located in
    directory: './migrations',
    tableName: 'migrations'
  }
};
