
/**
 * Constants
 * @private
 */
const TABLE_USERS = 'users';
const TABLE_SOCIALFEED_ACCOUNTS = 'socialfeed_accounts';
const TABLE_SOCIALFEEDS = 'socialfeeds';


exports.up = function(knex, Promise) {

  return Promise.all([

    knex.schema.createTableIfNotExists(TABLE_USERS, t => {

      t.increments().primary();

      // Email
      t.string('email').nullable().unique().index();
      t.string('password').nullable();

      // Social
      t.string('fb_id').nullable().unique();
      t.string('tw_id').nullable().unique();

      // Profile
      t.string('display_name', 50).nullable();
      t.string('first_name', 50).nullable();
      t.string('last_name', 50).nullable();
      t.string('profile_picture').nullable();
      t.string('gender', 1).nullable();

      t.text('bio').nullable();
      t.date('birth_date').nullable();

      // Password Reset
      t.string('password_reset_token').nullable();
      // t.dateTime('password_reset_expiry').nullable();

      // Book-keeping
      t.dateTime('created').notNull();
      t.dateTime('modified').nullable();

      t.string('fb_token').nullable();
      t.string('tw_token').nullable();
    }),

    knex.schema.createTableIfNotExists(TABLE_SOCIALFEED_ACCOUNTS, t => {

      t.increments().primary();

      t.string('social_type', 2).notNull();
      t.string('social_id').nullable();
      t.string('social_username').nullable();
      t.specificType('published','tinyint(1)').notNull().index();
      t.text('info').nullable();

      // Book-keeping
      t.dateTime('created').notNull();
      t.dateTime('modified').nullable();
    }),

    knex.schema.createTableIfNotExists(TABLE_SOCIALFEEDS, t => {

      t.increments().primary();

      // Socail feed
      t.string('social_type', 2).notNull().index();
      t.string('social_id').notNull().index();
      t.text('description').nullable();
      t.dateTime('social_created').notNull().index();
      t.string('link').notNull();
      t.string('image_url').nullable();
      t.string('video_url').nullable();
      t.string('video_format').nullable();

      // Social user
      t.string('social_user').notNull().index();

      // Cms Content
      t.specificType('published','tinyint(1)').notNull().index();
      t.specificType('rank','tinyint').notNull();

      // Book-keeping
      t.dateTime('created').notNull();
      t.dateTime('modified').nullable();
    })

  ]);
};

exports.down = function(knex, Promise) {

  return Promise.all([
    knex.schema.dropTable(TABLE_USERS)
  ]);
};
