
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2016)
 * @params {Number} query.limit (Default = 40)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    super_region_id: Joi.number().integer().min(2).max(100).default(2)
  }),


  /**
   * Handle request
   */
  (req, res, next) => {

    app.controllers.contestants.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
