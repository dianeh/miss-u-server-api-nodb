
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2016)
 * @params {Number} query.limit (Default = 40)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2016),// you can set current year by following command: (new Date).getFullYear()
    limit: Joi.number().integer().min(2).max(100).default(40)
  }),


  /**
   * Handle request
   */
  (req, res, next) => {
    
    app.controllers.contestants_v2.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
