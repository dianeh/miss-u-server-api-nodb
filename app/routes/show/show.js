
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.limit
 */
module.exports = [

  /**
   * Request validator
   */
 requestValidator({
    type: Joi.string()
  }, 'params'),

  /**
   * Handle request
   */
  (req, res, next) => {

    app.controllers.shows.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
