
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.offset (Default = 40)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),


  /**
   * Handle request
   */
  (req, res, next) => {



// If a particular gallery is requested
    if (req.params.id) {
      req.query.id = req.params.id;

      // Make 2 Db calls
      P = Promise.all([
        app.controllers.talent.getTalentById(req.params),
      ]);
    }
    else {
      P =  app.controllers.talent.get(req.query);
    }




    app.controllers.talent.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
