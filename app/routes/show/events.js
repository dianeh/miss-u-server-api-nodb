
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.limit
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    limit: Joi.number().integer().min(2).max(100).default(10),
  }),


  /**
   * Handle request
   */
  (req, res, next) => {

    app.controllers.events.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
