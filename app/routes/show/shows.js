
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.limit
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    limit: Joi.number().integer()
  }),

  requestValidator({
    type: Joi.string()
  }, 'params'),

  /**
   * Handle request
   */
  (req, res, next) => {

   var P;

    // If particular image url
    if (req.params.type) {
      req.query.type = req.params.type;
      // Make a Db call
      P = app.controllers.shows.getByType(req.query) ;
    } 
    else {
      P = app.controllers.shows.get(req.query);
    }

    P.then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
