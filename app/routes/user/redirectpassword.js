
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator'),
  requestAuth = require('../../../framework/request_auth');


/**
 * Constants
 * @private
 */


/**
 * @params {String} body.old_password
 * @params {String} body.new_password
  */
module.exports = [

  /**
   * Auth Request with role "user"
   */
/////

requestValidator({
    type: Joi.string()
  }, 'params'),

  /**
   * Handle request
   */

  /**
   * Request validator
   */


  /**
   * Handle request
   */
  (req, res, next) => {

    if (req.params.type) {
      req.query.type = req.params.type;
    }
    // Get Data From DB

      //return res.redirect('missu://resetpassword');
      return res.redirect('missu://resetpassword?token='+req.query.type);

  }
];
