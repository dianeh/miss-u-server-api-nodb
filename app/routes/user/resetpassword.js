
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');
  requestAuth = require('../../../framework/request_auth'),
  aes = require('../../../framework/aes');


/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * @params {String} body.token
 * @params {String} body.password
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    token: Joi.string(),
    password: Joi.string().min(6).label('Password')
  })
  .requiredKeys('token', 'password')),


  /**
   * Handle request
   */
  (req, res, next) => {

    var dbUser;

    app.controllers.user.verifyToken(req.body.token)

    .then(user => {

      if (user && user.id && user.token) {

        // Verify Data From DB
        return app.db_user
          .select('id','email','password_reset_token')
          .where({
            id: user.id,
            password_reset_token: user.token
          })
          .limit(1)
          .table(TABLE)
      }

      // User token is invalid
      return new Promise((r, reject) => {
        return reject('Token is invalid.');
      });
    })

    .then(userFound => {

      dbUser = userFound[0];
      dbUser = aes.decryptUser(dbUser);

      if (dbUser && dbUser.id) {
        return app.controllers.user.hashPassword(req.body.password);
      }

      return new Promise((r, reject) => {
        return reject('Token is invalid.');
      });
    })

    .then(hashedPassword => {

      if (hashedPassword) {
        return app.controllers.user.update(dbUser.id, {
          hashedPassword: hashedPassword,
          password_reset_token: null
        });
      }

      return new Promise((r, reject) => {
        return reject('Token is invalid.');
      });
    })

    .then(() => {
      return app.controllers.user.createToken(dbUser);
    })

    .then(retObj => {
      return res.success(retObj);
    })

    .catch(err => {
      var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });
  }
];
