
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');
  requestAuth = require('../../../framework/request_auth'),
  aes = require('../../../framework/aes');


/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * @params {String} body.old_password
 * @params {String} body.new_password
  */
module.exports = [

  /**
   * Auth Request with role "user"
   */
  requestAuth(['user','guest']),

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    current_password: Joi.string().min(6).label('Current Password'),
    new_password: Joi.string().min(6).label('New Password')
  })
  .requiredKeys('current_password', 'new_password')),


  /**
   * Handle request
   */
  (req, res, next) => {

    var dbUser;

    // Get Data From DB
    app.db_user
      .select('id','password')
      .where({ id: req.user.id })
      .limit(1)
      .table(TABLE)

    .then(user => {

      if (user && user.length &&
          user[0].password) {

        // Save user info
        dbUser = Object.assign({}, user[0]);
        dbUser = aes.decryptUser(dbUser);

        // Compare password
        return app.controllers.user.comparePassword(
          req.body.current_password, dbUser.password);
      }

      // User password not found
      return new Promise((r, reject) => {
        return reject('Password is not set, use forgot password to setup a password.');
      });
    })

    .then(passwordMatch => {

      if (passwordMatch === true && dbUser.id) {
        return app.controllers.user.hashPassword(req.body.new_password);
      }

      return new Promise((r, reject) => {
        return reject('Invalid current password.');
      });
    })

    .then(hashedPassword => {

      if (hashedPassword) {
        dbUser.hashedPassword = hashedPassword;
        return app.controllers.user.update(dbUser.id, dbUser);
      }

      return new Promise((r, reject) => {
        return reject('Invalid current password.');
      });
    })

    .then(retObj => {
      return res.success('Password changed.');
    })

    .catch(err => {

      var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });
  }
];
