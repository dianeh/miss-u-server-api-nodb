
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  sendgrid = require('sendgrid')(app.config.sendgrid),
  Joi = require('joi'),
  uuid = require('uuid'),
  Bitly = require('bitly'),
  util = require("util"),
  requestValidator = require('../../../framework/request_validator');
  requestAuth = require('../../../framework/request_auth'),
  aes = require('../../../framework/aes');


/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * @params {String} body.email
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    email: Joi.string().lowercase().trim()
              .email({ minDomainAtoms: 2 }).label('Email')
  })
  .requiredKeys('email')),


  /**
   * Handle request
   */
  (req, res, next) => {

    var dbUser, Token, link;

    // Get Data From DB
    app.db_user
      .select('id')
      .where({ email: aes.encrypt(req.body.email) })
      .limit(1)
      .table(TABLE)

    .then(user => {

      if (user && user.length &&
          user[0].id) {

        // Save user info
        dbUser = Object.assign({}, user[0], {
          password_reset_token: uuid.v4()
        });

        dbUser = aes.decryptUser(dbUser);

        // Compare password
        return app.controllers.user.signToken({
          id: dbUser.id,
          token: dbUser.password_reset_token
        });
      }

      // User password not found
      return new Promise((r, reject) => {
        return reject('Email was not found.');
      });
    })

    .then(token => {
      Token = token;
      link = `https://missuapi.nyfw.io/v1/redirectpassword/${Token}`;
      return app.controllers.user.update(dbUser.id, dbUser);
    })

    .then(updated => {
      var bitly = new Bitly(app.config.bitly);

      return bitly.shorten(link).then(response => {
        const maybeShortenedLink = response.data.url;
        if (maybeShortenedLink) {
          return maybeShortenedLink;
        }
        else {
          console.error("Received error response from bitly: " + util.inspect(response, {depth:null}));
          return link;
        }
      }, err => {
        console.error("Received error response from bitly: " + util.inspect(err, {depth:null}));
        return link;
      });
    })

    .then(emailLink => {

      return new Promise((resolve, reject) => {
        sendgrid.send({
          from: 'no-reply@missuniverse.com',
          to: req.body.email,
          subject: 'Reset your password.',
          html: `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Reset your password</title>
</head>
<body itemscope itemtype="http://schema.org/EmailMessage">
<table class="body-wrap">
  <tr>
    <td class="container">
      We have received a request to reset your password for the Miss U mobile app.
      <br /><br />
      Please click the link: <a href ='${emailLink}'>Password reset link</a>.
      <br /><br />
     OR copy the following link address and paste it in your mobile browser:
${emailLink} 
      <br /><br />
      Thank you for using the Miss U app. Be confidently beautiful!
    </td>
  </tr>
</table>
</body>
</html>`
        }, (err, ret) => {
          if (err) return reject(err);
          return resolve(ret);
        });

      });
    })

    .then(retObj => {
      return res.success('Email sent.');
    })

    .catch(err => {

	    var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });
  }
];
