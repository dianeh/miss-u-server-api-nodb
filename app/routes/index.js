
/**
 * Constants
 */
const API_VERSION = '/v1';
const API_VERSION_V2 = '/v2';


/**
 * Feed endpoints
 */

// Get all
app.server.get([`${API_VERSION}/feed/all`, `${API_VERSION_V2}/feed/all`],
  require('./feed/all'));

// Get videos
app.server.get([`${API_VERSION}/feed/videos`, `${API_VERSION_V2}/feed/videos`],
  require('./feed/videos'));

// Get videos
app.server.get([`${API_VERSION}/feed/getmembervideos/:name?`, `${API_VERSION_V2}/feed/getmembervideos/:name?`],
  require('./feed/membervideos'));

// Get news
app.server.get([`${API_VERSION}/feed/news`, `${API_VERSION_V2}/feed/news`],
  require('./feed/news'));

// Get photos
app.server.get([`${API_VERSION}/feed/photos/:id?`, `${API_VERSION_V2}/feed/photos/:id?`],
  require('./feed/galleries'));

// Get photos for a member
app.server.get([`${API_VERSION}/feed/getmemberphotos/:member?`, `${API_VERSION_V2}/feed/getmemberphotos/:member?`],
 require('./feed/memberphotos'));

// Get galleries
app.server.get([`${API_VERSION}/feed/galleries/:id?`, `${API_VERSION_V2}/feed/galleries/:id?`],
  require('./feed/galleries'));

// Get social
app.server.get([`${API_VERSION}/feed/social/:id?`, `${API_VERSION_V2}/feed/social/:id?`],
  require('./feed/social'));

// Get social
app.server.get([`${API_VERSION}/feed/getmembersocial/:member?`, `${API_VERSION_V2}/feed/getmembersocial/:member?`],
 require('./feed/membersocial'));

/**
 * Auth endpoints
 */

// Email auth
app.server.post([`${API_VERSION}/auth/email`, `${API_VERSION_V2}/auth/email`],
  require('./auth/email'));

// Guest auth
app.server.post([`${API_VERSION}/auth/guest`, `${API_VERSION_V2}/auth/guest`],
  require('./auth/guest'));

// Facebook auth
app.server.post([`${API_VERSION}/auth/:type(fb|tw)`, `${API_VERSION_V2}/auth/:type(fb|tw)`],
  require('./auth/social'));

// Email register
app.server.post([`${API_VERSION}/auth/register`, `${API_VERSION_V2}/auth/register`],
  require('./auth/register'));

/**
 * User endpoints
 */
// Change password
app.server.post([`${API_VERSION}/user/changepassword`, `${API_VERSION_V2}/user/changepassword`],
  require('./user/changepassword'));

// Forgot password
app.server.post([`${API_VERSION}/user/forgotpassword`, `${API_VERSION_V2}/user/forgotpassword`],
  require('./user/forgotpassword'));

// Reset password
app.server.post([`${API_VERSION}/user/resetpassword`, `${API_VERSION_V2}/user/resetpassword`],
  require('./user/resetpassword'));

app.server.get([`${API_VERSION}/show/shows/:type?`, `${API_VERSION_V2}/show/shows/:type?`],
  require('./show/shows'));

// Get getimages
app.server.get([`${API_VERSION}/feed/getappimages/:type?`, `${API_VERSION_V2}/feed/getappimages/:type?`],
  require('./feed/appimages'));

// Get redirect with token
app.server.get([`${API_VERSION}/redirectpassword/:type?`, `${API_VERSION_V2}/redirectpassword/:type?`],
  require('./user/redirectpassword'));

// Get events
app.server.get([`${API_VERSION}/show/events`, `${API_VERSION_V2}/show/events`],
  require('./show/events'));

// Get talent
app.server.get([`${API_VERSION}/show/talent/:id?`, `${API_VERSION_V2}/show/talent/:id?`],
  require('./show/talent'));

// Get contestants
app.server.get(`${API_VERSION}/show/contestants/:id?`,
  require('./show/contestants'));

// Get contestants
app.server.get(`${API_VERSION_V2}/show/contestants/:id?`,
	require('./show/contestants_v2'));