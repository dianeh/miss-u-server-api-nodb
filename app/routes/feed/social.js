
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),


  /**
   * Handle request
   */
  (req, res, next) => {

    app.controllers.social.get(req.query)

    .then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
