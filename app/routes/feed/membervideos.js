
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

 requestValidator({
    name: Joi.string()
  }, 'params'),

  /**
   * Handle request
   */
  (req, res, next) => {


   var P;

    // If particular videos of a name of a member (in descriptions) s requested
    if (req.params.name) {
      req.query.name = req.params.name;
      // Make a Db call
      P = app.controllers.videos.getVideosByMemberId(req.params) ;
    } 
    else {
    P = app.controllers.videos.get(req.query);
    }

    P.then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
