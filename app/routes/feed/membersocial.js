
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */


module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

  requestValidator({
    member: Joi.number().integer().min(1)
  }, 'params'),


  /**
   * Handle request
   */
  (req, res, next) => {


    var P;

    // If a particular gallery is requested
    if (req.params.member) {
      req.query.member = req.params.member;

      P = app.controllers.social.getSocialByMemberId(req.params);
    } else {
      P = app.controllers.social.get(req.query);
    }


    P.then(rows => {

      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }


];
