
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

  requestValidator({
    id: Joi.number().integer().min(1)
  }, 'params'),


  /**
   * Handle request
   */
  (req, res, next) => {

    var P;

    // If a particular gallery is requested
    if (req.params.id) {
      req.query.gallery_id = req.params.id;

      // Make 2 Db calls
      P = Promise.all([
        app.controllers.photos.getGalleryById(req.params),
        app.controllers.photos.get(req.query)
      ]);
    }
    else {
      P = app.controllers.photos.getGalleries(req.query);
    }


    P.then(rows => {

      // If a particular gallery is requested
      if (req.params.id) {
        rows = Object.assign(rows[0], {
          photos: rows[1]
        });
      }

      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
