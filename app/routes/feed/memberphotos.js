
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

  requestValidator({
    member: Joi.number().integer().min(1)
  }, 'params'),


  /**
   * Handle request
   */
  (req, res, next) => {


///////////
var P;

// If particular videos of a member_id is requested
    if (req.params.member) {
      req.query.member = req.params.member;

      // Make a Db call
      P = app.controllers.photos.getPhotosByMemberId(req.params);
    }
    else {
      P = app.controllers.photos.get(req.query);
    }

    P.then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });

  }
];
