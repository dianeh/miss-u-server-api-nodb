
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

 requestValidator({
    id: Joi.number().integer().min(1)
  }, 'params'),

  /**
   * Handle request
   */
  (req, res, next) => {

var P;

// If particular videos of a member_id is requested
    if (req.params.id) {
      req.query.member_id = req.params.id;

      // Make a Db call
      P = Promise.all([
        app.controllers.videos.getVideosByMemberId(req.params),
      ]);
    } 
    else {
    P = app.controllers.videos.get(req.query);
    }

    P.then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
