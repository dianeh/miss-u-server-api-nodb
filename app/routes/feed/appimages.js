
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(100).default(40),
    offset: Joi.number().integer().min(0)
  }),

 requestValidator({
    type: Joi.string()
  }, 'params'),

  /**
   * Handle request
   */
  (req, res, next) => {


   var P;

    // If particular image url
    if (req.params.type) {
      req.query.type = req.params.type;
      // Make a Db call
      P = app.controllers.appimages.getImageByType(req.params) ;
    } 
    else {
    P = app.controllers.appimages.get(req.query);
    }

    P.then(rows => {
      return res.success(rows);
    })
    .catch(err => {
      return next(err);
    });
  }
];
