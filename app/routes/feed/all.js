
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Default = 40)
 * @params {Number} query.photo_offset (Optional)
 * @params {Number} query.video_offset (Optional)
 * @params {Number} query.news_offset (Optional)
 */
module.exports = [

  /**
   * Request validator
   */
  requestValidator({
    year: Joi.number().integer().min(2012).default(2015),
    limit: Joi.number().integer().min(2).max(120).default(120),
    photo_offset: Joi.number().integer().min(0),
    video_offset: Joi.number().integer().min(0),
    news_offset: Joi.number().integer().min(0)
  }),


  /**
   * Handle request
   */
  (req, res, next) => {

    Promise.all([
      app.controllers.videos.get(Object.assign({}, req.query, { offset: req.query.video_offset })),
      app.controllers.photos.getGalleries(Object.assign({}, req.query, { offset: req.query.photo_offset })),
      app.controllers.news.get(Object.assign({}, req.query, { offset: req.query.news_offset }))
    ])

    .then(rows => {

      return res.success(_.chain(rows)
        .flatten() // Flatten Promise
        .map((r) => {
          // coalesce these fields into a single field for sorting
          r.pd = r.app_published_date || r.published_date;
          return r;
        })
        .orderBy(['rank'], ['asc'])
        .orderBy(['pd'], ['desc'])
        .map((r) => {
          // but don't return it to the client
          delete r.pd;
          return r;
        })
        .take(req.query.limit)
        .value());
    })
    .catch(err => {
      return next(err);
    });
  }
];
