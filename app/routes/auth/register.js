
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  joiHelpers = require('joi-helpers'),
  requestValidator = require('../../../framework/request_validator'),
  aes = require('../../../framework/aes');


/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * @params {String} body.email
 * @params {String} body.password
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    email: Joi.string().lowercase().trim()
              .email({ minDomainAtoms: 2 }).label('Email'),
    password: Joi.string().min(6).label('Password')
  })
  .requiredKeys('email', 'password')),


  /**
   * Handle request
   */
  (req, res, next) => {

    // Get Data From DB
    app.db_user
      .select('id')
      .where({ email: aes.encrypt(req.body.email) })
      .limit(1)
      .table(TABLE)

    .then(user => {

      if (user && user.length &&
          user[0].id) {

        user[0] = aes.decryptUser(user[0]);

        // User account exists
        return new Promise((r, reject) => {
          return reject('Email already exists, try to log in.');
        });
      }

      return app.controllers.user.register(req.body);
    })

    .then(retObj => {
      return res.success(retObj);
    })

    .catch(err => {

      var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });

  }
];
