
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  crypto = require('../../../framework/crypto'),
  requestValidator = require('../../../framework/request_validator'),
  aes = require('../../../framework/aes');


/**
 * @params {String} body.token
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    token: Joi.string().trim().required(),
    token_secret: Joi.string().trim()
  })),


  /**
   * Handle request
   */
  (req, res, next) => {

    // Social auth type - emum=[fb,tw]
    const type = req.params.type.toLowerCase();
    var dbUser;


    // Connect to FB or Tw
    app.controllers[type].connect(req.body)

    .then(socialUser => {

      // Save user info
      dbUser = Object.assign({}, socialUser);


      // Get Data From DB
      const dbQuery = app.db_user
        .select('id','email',`${type}_id`)
        .limit(1)
        .table('users');

      if (socialUser.email && socialUser[`${type}_id`]) {
        dbQuery.where({ email: aes.encrypt(socialUser.email) })
          .orWhere(`${type}_id`, aes.encrypt(socialUser[`${type}_id`]));
      }
      else if (socialUser[`${type}_id`]) {
        dbQuery.where(`${type}_id`, aes.encrypt(socialUser[`${type}_id`]));
      }
      else {
        dbQuery.where({ email: aes.encrypt(socialUser.email) })
      }

      return dbQuery;
    })

    .then(user => {

      if (user && user.length &&
          user[0].id) {

        user[0] = aes.decryptUser(user[0]);

        /**
         * Edge case:
         * If email aready exits, dont update
         */
        if (user[0].email) {
          delete dbUser.email;
        }

        // User account exists, update info
        return app.controllers.user.update(
          user[0].id,
          Object.assign({}, user[0], dbUser));
      }

      // User account not found
      return app.controllers.user.register(dbUser);
    })

    .then(retObj => {
      return res.success(retObj);
    })

    .catch(err => {

	    var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });

  }
];
