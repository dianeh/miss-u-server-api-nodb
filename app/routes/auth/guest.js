
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator');


/**
 * @params {String} body.id
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    id: Joi.string().lowercase().trim().required()
  })),


  /**
   * Handle request
   */
  (req, res, next) => {

    app.controllers.user.createToken({}, 'guest')

    .then(retObj => {
      return res.success(retObj);
    })

    .catch(err => {

      var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });

  }
];
