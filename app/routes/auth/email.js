
/**
 * Module dependencies.
 * @private
 */
const Joi = require('joi'),
  requestValidator = require('../../../framework/request_validator'),
  aes = require("../../../framework/aes");


/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * @params {String} body.email
 * @params {String} body.password
  */
module.exports = [

  /**
   * Request validator
   */
  requestValidator(Joi.object({
    email: Joi.string().lowercase().trim()
              .email({ minDomainAtoms: 2 }).label('Email'),
    password: Joi.string().min(6).label('Password')
  })
  .requiredKeys('email', 'password')
  .options({
    language: {
      any: { required: '!!Invalid email/password.' },
      string: { min: '!!Invalid email/password.' }
    }
  })),


  /**
   * Handle request
   */
  (req, res, next) => {

    var dbUser;

    // Get Data From DB
    app.db_user
      .select('id','email','password')
      .where({ email: aes.encrypt(req.body.email) })
      .limit(1)
      .table(TABLE)

    .then(user => {

      if (user && user.length &&
          user[0].email && user[0].password) {


        user[0] = aes.decryptUser(user[0]);

        // Save user info
        dbUser = Object.assign({}, user[0]);

        // Compare password
        return app.controllers.user.comparePassword(
          req.body.password, dbUser.password);
      }

      // User account not found
      return new Promise((r, reject) => {
        return reject('Invalid email/password.');
      });
    })

    .then(passwordMatch => {

      if (passwordMatch === true && dbUser.id) {
        return app.controllers.user.createToken(dbUser);
      }

      return new Promise((r, reject) => {
        return reject('Invalid email/password.');
      });
    })

    .then(retObj => {
      return res.success(retObj);
    })

    .catch(err => {
      var Err = null;
      if (err instanceof Error === false) {
        Err = new Error();
        Err.status = 400;
        Err.details = err;
      }

      return next(Err || err);
    });
  }
];
