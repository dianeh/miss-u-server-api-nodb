
const deepFreeze = require('deep-freeze'),
  extend = require('extend');


const defaults = {

  server: {
    port: process.env.NODE_PORT || process.env.PORT || 9999
  },

  jwtSecret: process.env.JWT_SECRET || 'b7o<GH1).u%n8d2[nfzio95kHcL2qT',

  sendgrid: process.env.SENDGRID,

  aws: {
    cf_url: process.env.AWS_CF_URL
  },

  fb: {
    id: process.env.FB_ID,
    secret: process.env.FB_SECRET,
  },

  bitly:  process.env.BITLY_TOKEN,

  tw: {
    id: process.env.TW_ID,
    secret: process.env.TW_SECRET,
  },

  mysql_user: {
    // Connection
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASS,
    database: process.env.MYSQL_DB,

    // Connection pool
    pool: {
      min: 2,
      max: 10
    },

    // Enable debug in test
    debug: true,

    // Table to build schema
    tables: [
      'users',
      'socialfeed_accounts',
      'socialfeeds'
    ]
  },

  mysql_com: {
    // Connection
    host: process.env.MYSQL_COM_HOST,
    user: process.env.MYSQL_COM_USER,
    password: process.env.MYSQL_COM_PASS,
    database: process.env.MYSQL_COM_DB,

    // Connection pool
    pool: {
      min: 2,
      max: 10
    },

    // Enable debug in test
    debug: true,

    // Table to build schema
    tables: [
      'contestant_profiles',
      'galleries',
      'media_associations',
      'members',
      'news',
      'ph_photos',
      'photos',
      'you_videos'
    ]
  },

  crypto_key: process.env.CRYPTO_KEY
};


const production = {

  mysql_com: {
    // Disable debug in prod
    debug: false
  },
  mysql_user: {
    // Disable debug in prod
    debug: false
  }
};


module.exports = env => {
  return deepFreeze(extend(
    true,
    { env: env },
    (defaults || {}),
    (env === 'production' ? production : {})
  ));
};
