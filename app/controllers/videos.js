
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE = 'you_videos';
//const TABLE_MA = 'media_associations';
const TABLE_M = 'members';
const FILEDS = [
  'id',
  'name',
  'description',
  'year',
  'published_date',
  'created',
  'modified',
  'video_Id',
  'video_url', // Added
  'image_url', // Added
  'type', // Added
  'rank', // ToDo
  'img_height',
  'img_width'
];


/**
 * @params {Number} query.year
 * @params {Number} query.limit
 * @params {Number} query.offset (Optional)
 *
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_com.select(`missu.${TABLE}.*`,
                                    `missu_user.image_dimentions.height AS img_height`,
                                    `missu_user.image_dimentions.width AS img_width`)
    .where('year', '>=', query.year)
    // Only published
    .where('app_published', '=', 1)
    .orderBy('rank', 'asc')
    .orderBy('published_date', 'desc')
    .limit(query.limit)
    .table(TABLE)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 1);
    });

  // Build query
  query.offset &&
  dbQuery.offset(query.offset);

  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('video_Id')
        .map(i => {
          i.type = 'videos';
          i.image_url = `${app.config.aws.cf_url}you_video/thumbnails/${i.thumbnail}`;
          i.video_url = i.url;

          if (i.img_width === null || i.img_height === null) {
            delete i.img_width;
            delete i.img_height;
          }

          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};

/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getVideosByMemberId = query => {

  var name_search = query.name.trim();
  name_search = name_search.split('_').join(' ');
  name_search = '%'+name_search+'%';  

  // Get Data From DB
  const dbQuery = app.db_com.select(`missu.${TABLE}.*`,
                                    `missu_user.image_dimentions.height AS img_height`,
                                    `missu_user.image_dimentions.width AS img_width`)

//   .where('year', '>=', query.year)
    .where('rank', '<', 100)
    .orderBy('rank', 'asc')
    .orderBy('published_date', 'desc')
    .limit(query.limit);

  var contestant = query.name.toLowerCase().trim().split('_').join(' ');
  if (contestant == "pia alonzo wurtzbach" || contestant == "pia wurtzbach") {
    dbQuery
      .where('video_category_id', '=', 39)
      .where('published_date', '>=', '2015-09-01')
      .orWhere(`${TABLE}.description`, 'LIKE', name_search)
      .orWhere(`${TABLE}.name`, 'LIKE', name_search);
  }
  else if (contestant == "olivia jordan") {
    dbQuery
      .where('video_category_id', '=', 37)
      .where('published_date', '>=', '2015-09-01')
      .where('year', '=', 2015)
      .orWhere(`${TABLE}.description`, 'LIKE', name_search)
      .orWhere(`${TABLE}.name`, 'LIKE', name_search);
  }
  else if (contestant == "katherine haik") {
    dbQuery
      .where('video_category_id', '=', 42)
      .where('published_date', '>=', '2015-09-01')
      .orWhere(`${TABLE}.description`, 'LIKE', name_search)
      .orWhere(`${TABLE}.name`, 'LIKE', name_search);
  }
  else {
    dbQuery.where(function () {
        this.where(`${TABLE}.description`, 'LIKE', name_search).orWhere(`${TABLE}.name`, 'LIKE', name_search);
    });
  }
  
  dbQuery
    .table(TABLE)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 1);
    });

  //query.last_id &&
  //dbQuery.where('id', '<', query.last_id);

  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('video_Id')
        .map(i => {
          i.type = 'videos';
          i.image_url = `${app.config.aws.cf_url}you_video/thumbnails/${i.thumbnail}`;
          i.video_url = i.url;
          if (i.img_width === null || i.img_height === null) {
            delete i.img_width;
            delete i.img_height;
          }
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};
