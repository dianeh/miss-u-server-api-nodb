
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  joiHelpers = require('joi-helpers'),
  crypto = require('../../framework/crypto'),
  aes = require("../../framework/aes");

/**
 * Constants
 * @private
 */
const TABLE = 'users';


/**
 * Insert User in DB
 * @params {Object} dbUser
 * @private
 */
function insertUser(dbUser) {

  // Prepare insert data
  dbUser = Object.assign({}, _.pick(dbUser, [
    'email',
    'hashedPassword',
    'first_name',
    'last_name',
    'profile_picture',
    'gender',
    'fb_id',
    'tw_id',
    'display_name',
  ]), {
    id: 0,
    modified: new Date(),
    created: new Date(),
  });

  dbUser = aes.encryptUser(dbUser);

  // Password cleanup
  // if (dbUser.hashedPassword) {
  dbUser.password = dbUser.hashedPassword;
  delete dbUser.hashedPassword;
  // }

  if (dbUser.gender === '') {
    delete dbUser.gender;
  }


  // Validate against schema
  const validate = joiHelpers.validate(
    app.schema[TABLE], dbUser);


  // Validation failed
  if (validate.error) {
    return new Promise((r, reject) => {
      return reject(validate.error);
    });
  }

  // Cleanup
  delete validate.value.id;

  // Insert user & Return Promise
  return app.db_user
    .insert(validate.value)
    .table(TABLE);
};


/**
 * Update User in DB
 * @params {String} id (Required)
 * @params {Object} dbUser
 * @private
 */
function updateUser(id, dbUser) {

  // Prepare insert data
  dbUser = Object.assign({}, _.pick(dbUser, [
    'email',
    'first_name',
    'last_name',
    'profile_picture',
    'gender',
    'fb_id',
    'tw_id',
    'fb_token',
    'tw_token',
    'display_name',
    'password_reset_token',
    'password_reset_expirty',
    'hashedPassword' // For password update
  ]), {
    id: 0,
    modified: new Date(),
    created: new Date(),
  });

  dbUser = aes.encryptUser(dbUser);

  // Password cleanup
  if (dbUser.hashedPassword) {
    dbUser.password = dbUser.hashedPassword;
    delete dbUser.hashedPassword;
  }

  if (dbUser.gender === '') {
    delete dbUser.gender;
  }

  // Validate against schema
  const validate = joiHelpers.validate(
    app.schema[TABLE], dbUser);


  // Validation failed
  if (validate.error) {
    return new Promise((r, reject) => {
      return reject(validate.error);
    });
  }

  // Cleanup
  delete validate.value.id;
  delete validate.value.created;


  // Update user & Return Promise
  return app.db_user
    .where({ id: id })
    .update(validate.value)
    .table(TABLE);
};


/**
 * Alias
 * @return Promise
 */
exports.hashPassword = crypto.hash;
exports.signToken = crypto.signToken;
exports.verifyToken = crypto.verifyToken;

/**
 * Inser User in DB
 * @params {Number} dbUser.id (Required)
 * @params {String} role = [user|guest]
 *
 * @return Promise
 */
exports.createToken = (dbUser, role) => {

  return crypto.signToken({
    id: dbUser.id || 0,
    role: role||'user'
  })

  .then(token => {

    const ret = Object.assign({
      token: token
    },
    _.chain(dbUser)
     .pick([
        'id',
        'email',
        'first_name',
        'last_name',
        'display_name',
        'gender',
        // 'profile_picture',
        // 'fb_id',
        // 'tw_id'
      ])
     .pickBy(v => v !== null)
     .value()
    );

    return new Promise(resolve => {
      return resolve(ret);
    });
  });
};


/**
 * @params {String} password1
 * @params {String} password2
 *
 * @return Promise
 */
exports.comparePassword = (p1, p2) => {
  return crypto.compare(p1, p2);
};


/**
 * @params {String} query.email
 * @params {String} query.password
 * @params {String} query.fb_id
 * @params {String} query.tw_id
 *
 * @return Promise
 */
exports.register = query => {

  var dbUser;
  var promise;


  /**
   * Case #1: email & password
   */
  if (query.email && query.password) {

    // Use brypt to encrypt password
    promise = crypto.hash(query.password)

    .then(hashedPassword => {

      // Prepare insert data
      dbUser = Object.assign({}, query, {
        hashedPassword: hashedPassword
      });

      // Insert user
      return insertUser(dbUser);
    });
  }

  /**
   * Case #2: twitter & facebook
   */
  else if (query.fb_id || query.tw_id) {

    // Prepare insert data
    dbUser = Object.assign({}, query);

    // Insert user
    promise = insertUser(dbUser);
  }


  return promise
  .then(id => {

    // Create JWT token
    if (id && id.length &&
        typeof id[0] !== 'undefined') {

      dbUser.id = id[0];
      return exports.createToken(dbUser);
    }

    return new Promise((r, reject) => {
      return reject(new Error());
    });

  });
};


/**
 * @params {String} id (Required)
 * @params {Object} dbUser
 *
 * @return Promise
 */
exports.update = (id, dbUser) => {

  return updateUser(id, dbUser)

  .then(updated => {

    // Create JWT token
    if (updated) {

      dbUser.id = id;
      return exports.createToken(dbUser);
    }

    return new Promise((r, reject) => {
      return reject(new Error());
    });

  });
};
