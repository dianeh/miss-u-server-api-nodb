
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  request = require('request').defaults({
    baseUrl: 'https://api.twitter.com/1.1/'
  });


/**
 * Constants
 */
const STREAM_LIMIT = 100;


/**
 * @params {String} opts.token
 * @params {String} opts.token_secret
 * @return {Promise}
 */
exports.connect = opts => {

  return new Promise((resolve, reject) => {

    request('/account/verify_credentials.json', {
      json: true,
      oauth: {
        consumer_key: app.config.tw.id,
        consumer_secret: app.config.tw.secret,
        token: opts.token,
        token_secret: opts.token_secret
      }
    },

    (err, res, body) => {

      console.info(opts);
      console.info(body);

      if (err) return reject(err);


      if (body && body.id_str && body.screen_name &&
          typeof body.errors === 'undefined') {

        // Splitting up fullname
        const name = body.name ? body.name.split(' ') : undefined;

        return resolve({
          tw_id: String(body.id_str),
          email: body.email ? body.email.toLowerCase() : undefined,
          display_name: body.screen_name.toLowerCase(),
          first_name: name ? name[0] : name,
          last_name: name && name.length > 1 ? name.splice(-1)[0] : undefined,
          profile_picture: body.profile_image_url,
          tw_token: opts.token +";;;"+ opts.token_secret
        });
      }

      // Invalid Permission
      return reject((body.errors && body.errors[0]) ||
        'Invalid token/token_secret.');
    });

  });
};
