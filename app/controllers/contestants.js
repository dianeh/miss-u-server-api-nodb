
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');

/**
 * Constants
 * @private
 */

//select cp.region_title, members.display_name, members.id, members.profile_picture, members.hometown, members.birth_date, members.full_bio, cp.height, cp.instagram, cp.twitter_user, cp.facebook
//FROM missu.members, missu.contestant_profiles cp
//WHERE members.id=cp.member_id and member_id=825069;

const TABLE_MEMBERS = 'members';
const TABLE_CP = 'contestant_profiles';
const FILEDS = [
  'id',
  'super_region_id',
  'year',
  'display_name',
  'first_name',
  'last_name',
  'region_title',
  'profile_picture',
  'profile_banner',
  'profile_header',
  'hometown',
  'birth_date',
  'full_bio',
  'height',
  'instagram',
  'twitter_user',
  'facebook',
  'funfact1',
  'funfact2',
  'funfact3',
  'movie',
  'song',
  'sport',
  'food',
  'results'
];



/**
 * @params {Number} query.year (Optional)
 * @params {Number} query.limit (Optional)
 * @params {Number} query.member_id (Optional)
 *
 * @return Promise
 */
exports.get = query => {
/*
'id',
  'display_name',
  'first_name',
  'last_name',
  'region_title',
  'profile_picture',
  'hometown',
  'birth_date',
  'full_bio',
  'height',
  'instagram',
  'twitter_user',
  'facebook'
*/
  // Get Data From DB
  const dbQuery = app.db_com.select(
      `${TABLE_MEMBERS}.id`,
      `${TABLE_MEMBERS}.super_region_id`,
      'contestant_profiles.year',
      'display_name',
      'first_name',
      'last_name',
      'region_title',
      'profile_picture',
      'profile_banner',
      'hometown',
      'birth_date',
      'full_bio',
      'height',
      'instagram',
      'twitter_user',
      'facebook',
      'funfact1',
      'funfact2',
      'funfact3',
      'movie',
      'song',
      'sport',
      'food',
      'results')
    .orderBy('members.id')
    .leftJoin('missu_user.results', 'missu_user.results.member_id', `${TABLE_MEMBERS}.id`)
    .innerJoin(TABLE_CP,`${TABLE_CP}.member_id`,`${TABLE_MEMBERS}.id`)
    .table(TABLE_MEMBERS);


  // Build query

  query.member_id &&
  dbQuery.where('members.id', query.member_id);

  query.year &&
  dbQuery.where('contestant_profiles.year','>', query.year);

    //  query.member_id &&
    query.super_region_id &&
    dbQuery.where('members.super_region_id', query.super_region_id)
   .orWhere('members.id',662306) 
   .orWhere('members.id', 662855)
   .orWhere(function () {
     this.where('members.id', 662174).where('members.year', 2015);
   })
   .orWhere('members.id', 826000);
//   .orWhereNotNull('missu_user.results.results');

  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        //.uniqBy('profile_picture')
        .uniqBy('id')
        .map(i => {
        //i.region_title = 'state is here';
       i.profile_picture = `${app.config.aws.cf_url}profiles/${i.profile_picture}`;
       i.profile_banner = `${app.config.aws.cf_url}profiles/${i.profile_banner}`;
       i.profile_header = i.profile_banner;

    return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};



/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getCPById = query => {

  // Get Data From DB
  return app.db_com.select(`${TABLE_members}.*`)
    .limit(1)
    .table(TABLE_MEMBERS)

  .then(rows => {

    return new Promise(res => {

      return res(
       _.chain(rows)
        .map(i => {
       i.profile_picture = `${app.config.aws.cf_url}profiles/${i.profile_picture}`;
       i.profile_banner = `${app.config.aws.cf_url}profiles/${i.profile_banner}`;
       i.profile_header = i.profile_banner;
          return _.pick(i, FILEDS);
        })
        .first()
        .value());
    });

  });
};
