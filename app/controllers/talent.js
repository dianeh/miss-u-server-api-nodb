
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE_MEMBERS = 'members';
const FILEDS = [
  'id',
  'member_type',
  'display_name',
  'profile_picture',
  'mini_bio',
  'created',
  'modified'
];


/**
 * @params {Number} query.year (Optional)
 * @params {Number} query.limit (Optional)
 * @params {Number} query.talent_id (Optional)
 *
 * @return Promise
 */

exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_com.select()
    // .orderBy('rating', 'desc')

//select member_type, display_name, profile_picture, mini_bio from members where super_region_id=2 and year=2015 and member_type='talent' OR super_region_id=2 and year=2015 and member_type='host' OR super_region_id=2 and year=2015 and member_type='judge' and title='Telecast Judge' order by member_type;

//    .where(`${TABLE_MEMBERS}.member_type`, 'judge')
  .whereIn(`${TABLE_MEMBERS}.member_type`, ['host','talent','judge'])
//    .where(`${TABLE_MEMBERS}.member_type`, 'Telecast Judge')
    .where(`${TABLE_MEMBERS}.super_region_id`, 3)
    .where(`${TABLE_MEMBERS}.year`, '2016')
    .orderBy('member_type', 'id')
    .table(TABLE_MEMBERS);

  // Build query

//  query.talent_id &&
//  dbQuery.where('talent_id', query.talent_id);

  query.limit &&
  dbQuery.limit(query.limit);

  // ToDo: Dependency
  //query.year &&
  // dbQuery.where('year', '>=', query.year);


  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('id')
        .map(i => {
//  i.profile_picture = `${app.config.aws.cf_url}photos/galleries/${i.profile_picture}`;
 i.profile_picture = `${app.config.aws.cf_url}profiles/${i.profile_picture}`;
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};




/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getTalentById = query => {

  // Get Data From DB
  return app.db_com.select(`${TABLE_MEMBERS}.*`)
    .where(`${TABLE_MEMBERS}.id`, query.id)
    .where(`${TABLE_MEMBERS}.year`, '2016')
    .limit(1)
    //.table(TABLE_GAL)

  .then(rows => {

    return new Promise(res => {

      return res(
       _.chain(rows)
        .map(i => {
          return _.pick(i, FILEDS);
        })
        .first()
        .value());
    });

  });
};
