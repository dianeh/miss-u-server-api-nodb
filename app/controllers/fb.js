
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  async = require('async'),
  request = require('request').defaults({
    baseUrl: 'https://graph.facebook.com/v2.5/'
  });


/**
 * Constants
 */
const STREAM_LIMIT = 100;


/**
 * @params {String} opts.token
 * @return {Promise}
 */
exports.connect = opts => {

  return new Promise((resolve, reject) => {

    request('/me', {
      json: true,
      qs: {
        debug: 'all',
        access_token: opts.token,
        fields: 'id,email,first_name,last_name,gender,picture'
      }
    },

    (err, res, body) => {

      console.info(opts);
      console.info(body);

      if (err) {
        // try again without email
        request('/me', {
          json: true,
          qs: {
            debug: 'all',
            access_token: opts.token,
            fields: 'id,first_name,last_name,gender,picture'
          }
        }, (err, res, body) => {
          if (err) {
            return reject(err);
          }

          if (body && body.id && body.email &&
              typeof body.error === 'undefined') {

            return resolve({
              fb_id: String(body.id),
              first_name: body.first_name,
              last_name: body.last_name,
              gender: (body.gender || '').charAt(0).toLowerCase(),
              profile_picture: body.picture.data.url,
              fb_token: opts.token
            });
          }

          // Invalid Permission
          return reject( (body.error && body.error) ||
            (body.__debug__ && body.__debug__.messages)||
            'Invalid token, or email permission not granted.');
        });
      }
      else {

        if (body && body.id && body.email &&
            typeof body.error === 'undefined') {

          return resolve({
            fb_id: String(body.id),
            email: body.email.toLowerCase(),
            first_name: body.first_name,
            last_name: body.last_name,
            gender: (body.gender || '').charAt(0).toLowerCase(),
            profile_picture: body.picture.data.url,
            fb_token: opts.token
          });
        }

        // Invalid Permission
        return reject( (body.error && body.error) ||
          (body.__debug__ && body.__debug__.messages)||
          'Invalid token, or email permission not granted.');
      }
    });
  });
};
