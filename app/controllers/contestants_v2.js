"use strict";
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');

/**
 * Constants
 * @private
 */

//select cp.region_title, members.display_name, members.id, members.profile_picture, members.hometown, members.birth_date, members.full_bio, cp.height, cp.instagram, cp.twitter_user, cp.facebook
//FROM missu.members, missu.contestant_profiles cp
//WHERE members.id=cp.member_id and member_id=825069;

const TABLE_MEMBERS = 'members';
const TABLE_CP = 'contestant_profiles';
const TABLE_AL = 'app_layout';
const FILEDS = [
  'id',
  'super_region_id',
  'year',
  'display_name',
  'first_name',
  'last_name',
  'region_title',
  'profile_picture',
  'profile_banner',
  'profile_header',
  'hometown',
  'birth_date',
  'full_bio',
  'height',
  'instagram',
  'twitter_user',
  'facebook',
  'funfact1',
  'funfact2',
  'funfact3',
  'movie',
  'song',
  'sport',
  'food',
  'results'
];



/**
 * @params {Number} query.year (Optional)
 * @params {Number} query.limit (Optional)
 * @params {Number} query.member_id (Optional)
 *
 * @return Promise
 */
exports.get = query => {
/*
'id',
  'display_name',
  'first_name',
  'last_name',
  'region_title',
  'profile_picture',
  'hometown',
  'birth_date',
  'full_bio',
  'height',
  'instagram',
  'twitter_user',
  'facebook'
*/

    return new Promise(resolve => {
        app.db_user.select('competition', 'super_region', 'year', 'layout', 'result_column').table(TABLE_AL).then(rows => {
            resolve(rows);
        });
    }).then(appLData => {
        // Get Data From DB
        const dbQuery = app.db_com.select(
            `${TABLE_MEMBERS}.id`,
            `${TABLE_MEMBERS}.super_region_id`,
            'contestant_profiles.year',
            'display_name',
            'first_name',
            'last_name',
            'region_title',
            'profile_picture',
            'profile_banner',
            'hometown',
            'birth_date',
            'full_bio',
            'height',
            'instagram',
            'twitter_user',
            'facebook',
            'funfact1',
            'funfact2',
            'funfact3',
            'movie',
            'song',
            'sport',
            'food',
            'results')
            .orderBy('members.id')
            .leftJoin('missu_user.results', 'missu_user.results.member_id', `${TABLE_MEMBERS}.id`)
            .innerJoin(TABLE_CP, `${TABLE_CP}.member_id`, `${TABLE_MEMBERS}.id`)
            .table(TABLE_MEMBERS);

        let response = {};
        let temp = {
            sql: []
        };
        // Build query
        appLData.forEach(i => {
            response[i.competition] = {
                layout_type: i.layout,
                winner: null,
                contestants: []
            };
            temp[i.super_region] = {
                response: response[i.competition],
                result_column: i.result_column,
                year: i.year,
            };
            temp.sql.push(i.result_column);

            dbQuery.orWhere(function () {
                this.where('contestant_profiles.year', i.year)
                    .andWhere(`${TABLE_MEMBERS}.super_region_id`, i.super_region);
            });
        });
        dbQuery.orWhere('results', 'IN', temp.sql);
        return dbQuery.then(rows => {
            return new Promise(resolve => {
                rows.forEach(i => {
                    _makePhoto(i);
                    if (temp[i.super_region_id]) {
                        let current = temp[i.super_region_id];
                        resolveEntry(i, current.result_column, current.response, current.year);
                    }
                });
                return resolve(response);
            });
        });
    });
};

function _makePhoto(i) {
    i.profile_picture = `${app.config.aws.cf_url}profiles/${i.profile_picture}`;
    i.profile_banner = `${app.config.aws.cf_url}profiles/${i.profile_banner}`;
    i.profile_header = i.profile_banner;
}
function resolveEntry(i, result, response, year) {
    if (i.results === result) {
        response.winner = _.pick(i, FILEDS);
        if (i.year == year) {
            response.contestants.push(_.pick(i, FILEDS));
        }
    } else {
        response.contestants.push(_.pick(i, FILEDS));
    }
}
/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getCPById = query => {

  // Get Data From DB
  return app.db_com.select(`${TABLE_members}.*`)
    .limit(1)
    .table(TABLE_MEMBERS)

  .then(rows => {

    return new Promise(res => {

      return res(
       _.chain(rows)
        .map(i => {
       i.profile_picture = `${app.config.aws.cf_url}profiles/${i.profile_picture}`;
       i.profile_banner = `${app.config.aws.cf_url}profiles/${i.profile_banner}`;
       i.profile_header = i.profile_banner;
          return _.pick(i, FILEDS);
        })
        .first()
        .value());
    });

  });
};
