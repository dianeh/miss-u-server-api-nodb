
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE = 'shows';
const FILEDS = [
  'id',    //added for now
  'type',
  'image_url',
  'target_url',
  'live_feed',
  'created'    /// added for now
  // 'rank', // ToDo
];


/**
 *   shows returns list of shows from shows table
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .table(TABLE);

  if (query.limit) dbQuery.limit(query.limit)

  return dbQuery
  // Send only unique results - they all are, just keep like this for now
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('id')
        .map(i => {
          //i.type = 'shows';
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};

exports.getByType = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .table(TABLE)
    .where('type', '=', query.type);

  if (query.limit) dbQuery.limit(query.limit)

  return dbQuery
  // Send only unique results - they all are, just keep like this for now
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('id')
        .map(i => {
          //i.type = 'shows';
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};