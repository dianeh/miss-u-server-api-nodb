
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE_FEEDS = 'socialfeeds';
const TABLE_SFA = 'socialfeed_accounts';
const FILEDS = [
  'id',
  'link',
  'description',
  'social_created',
  'modified',
  'social_type',
  'social_username',
  'image_url',
  'video_url',
  'video_format',
  'rank', // ToDo
  'img_height',
  'img_width'
];


/**
 * @params {Number} query.limit
 * @params {Number} query.offset (Optional)
 *
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .distinct('description', 'socialfeeds.id',
  'socialfeeds.link',
  'socialfeeds.description',
  'socialfeeds.social_created',
  'socialfeeds.modified',
  'socialfeeds.social_type',
  'socialfeed_accounts.social_username',
  'socialfeeds.image_url',
  'socialfeeds.video_url',
  'socialfeeds.video_format',
  'rank',
  'image_dimentions.width as img_width',
  'image_dimentions.height as img_height')   

    .where('socialfeeds.published', 1)
    .innerJoin(TABLE_SFA, function () {
      this
        .on('socialfeeds.social_user', '=', 'socialfeed_accounts.social_id')
        .andOn('socialfeeds.social_type', '=', 'socialfeed_accounts.social_type')
    })
    .leftJoin(`image_dimentions`, function () {
      this.on(`image_dimentions.item_id`, '=', `${TABLE_FEEDS}.id`)
          .andOn(`image_dimentions.table_id`, '=', 3);
    })
    .whereIn('socialfeed_accounts.member_id', [662174, 662306, 662855])
    .orderBy('rank', 'asc')
    .orderBy('social_created', 'desc')
    .limit(query.limit)
    .table(TABLE_FEEDS);

  // Build query
  query.offset &&
  dbQuery.offset(query.offset);

  return dbQuery
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .map(i => {
          i.type = 'social';
          i.created = i.social_created;
          i.description = i.description || '';
          return _.omitBy(_.pick(i, FILEDS), _.isNull);
        })
        .value());
    });

  });
};

/**
 * @params {Number} query.limit
 * @params {Number} query.offset (Optional)
 *
 * @return Promise
 */
exports.getSocialByMemberId = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .distinct('description', 'socialfeeds.id',
  'socialfeeds.link',
  'socialfeeds.description',
  'socialfeeds.social_created',
  'socialfeeds.modified',
  'socialfeeds.social_type',
  'socialfeed_accounts.social_username',
  'socialfeeds.image_url',
  'socialfeeds.video_url',
  'socialfeeds.video_format',
  'rank',
  'image_dimentions.width as img_width',
  'image_dimentions.height as img_height')

    .where('socialfeeds.published', 1)
    .orderBy('rank', 'asc')
    .orderBy('social_created', 'desc')
    .innerJoin('socialfeed_accounts','socialfeed_accounts.social_id' , 'socialfeeds.social_user')
    .leftJoin(`image_dimentions`, function () {
      this.on(`image_dimentions.item_id`, '=', `${TABLE_FEEDS}.id`)
          .andOn(`image_dimentions.table_id`, '=', 3);
    })
    .where(`${TABLE_SFA}.member_id`, query.member)
    //.where(`${TABLE_FEEDS}.social_type`, 'ig')
    .limit(40)
    .table(TABLE_FEEDS);
  // Build query
//  query.last_id &&
//  dbQuery.where('id', '<', query.last_id);


  return dbQuery
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .map(i => {
          i.type = 'social';
          i.created = i.social_created;
          i.description = i.description || '';
          return _.omitBy(_.pick(i, FILEDS), _.isNull);
        })
        .value());
    });

  });

};

