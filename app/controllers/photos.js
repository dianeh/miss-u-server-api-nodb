
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE_MA = 'media_associations';
const TABLE_PIC = 'photos';
const TABLE_GAL = 'galleries';
const FILEDS = [
  'id',
  'gallery_id',
  'year',
  'name',
  'description',
  'published_date',
  'created',
  'modified',
  'image_url', // Added
  'type', // Added
  'rank', // ToDo
  'img_height',
  'img_width'
];


/**
 * @params {Number} query.year (Optional)
 * @params {Number} query.limit (Optional)
 * @params {Number} query.offset (Optional)
 * @params {Number} query.gallery_id (Optional)
 *
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_com.select(`missu.${TABLE_PIC}.*`,
                                    'missu_user.image_dimentions.width as img_width',
                                    'missu_user.image_dimentions.height as img_height')
    // .orderBy('rating', 'desc')
    //.where('app_published', 1) No published field in photos table
    //.orderBy('rank', 'asc') No rank in photos table
    .table(TABLE_PIC)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE_PIC}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 4);
    });

  // Build query
  if (query.gallery_id) {

    dbQuery.orderByRaw(`(case when ${TABLE_PIC}.description = 'Headshot' then 0 else 1 end) asc`)
    dbQuery.where('gallery_id', query.gallery_id);
  }

  query.limit &&
  dbQuery.limit(query.limit);

  query.offset &&
  dbQuery.offset(query.offset);

  dbQuery.orderBy('created', 'desc')
  // ToDo: Dependency
  // query.year &&
  // dbQuery.where('year', '>=', query.year);


  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('file_name')
        .map(i => {
          i.image_url = `${app.config.aws.cf_url}photos/galleries/${i.file_name}`;
          i.type = 'photos';
          if (i.img_height === null || i.img_width === null) {
            delete i.img_height;
            delete i.img_width;
          }
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};


/**
 * @params {Number} query.year (Default = 2015)
 * @params {Number} query.limit (Optional)
 * @params {Number} query.offset (Optional)
 *
 * @return Promise
 */
exports.getGalleries = query => {

  // Get Data From DB
  const dbQuery = app.db_com.select(`${TABLE_GAL}.*`,
                                    `${TABLE_PIC}.file_name`,
                                    'missu_user.image_dimentions.width as img_width',
                                    'missu_user.image_dimentions.height as img_height')
    // Year
    .where(`${TABLE_GAL}.year`, '>=', query.year)
    // Only published
    .where(`${TABLE_GAL}.app_published`, 1)
    .whereNotIn(`${TABLE_GAL}.gallery_category_id`, [11, 12, 13])
    .orderBy(`${TABLE_GAL}.rank`, 'asc')
    .orderBy(`${TABLE_GAL}.published_date`, 'desc')
    .join(TABLE_PIC,`${TABLE_GAL}.thumbnail_photo_id`,`${TABLE_PIC}.id`)
    .table(TABLE_GAL)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE_PIC}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 4);
    });

  query.limit &&
  dbQuery.limit(query.limit);


  // Build query
  query.offset &&
  dbQuery.offset(query.offset);

  return dbQuery

  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('file_name')
        .map(i => {
          i.type = 'photos';
          i.image_url = `${app.config.aws.cf_url}photos/galleries/${i.file_name}`;
          if (i.img_height === null || i.img_width === null) {
            delete i.img_height;
            delete i.img_width;
          }
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};


/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getGalleryById = query => {

  // Get Data From DB
  return app.db_com.select(`${TABLE_GAL}.*`,
                           `${TABLE_PIC}.file_name`,
                           'missu_user.image_dimentions.width as img_width',
                           'missu_user.image_dimentions.height as img_height')
    .where(`${TABLE_GAL}.id`, query.id)
    .where(`${TABLE_GAL}.app_published`, 1)
    .orderBy(`${TABLE_GAL}.rank`, 'asc')
    .orderBy(`${TABLE_GAL}.published_date`, 'desc')
    .rightJoin(TABLE_PIC,`${TABLE_GAL}.thumbnail_photo_id`,`${TABLE_PIC}.id`)
    .limit(1)
    .table(TABLE_GAL)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE_PIC}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 4);
    })

  .then(rows => {

    return new Promise(res => {

      return res(
       _.chain(rows)
        .map(i => {
          i.image_url = `${app.config.aws.cf_url}photos/galleries/${i.file_name}`;
          if (i.img_height === null || i.img_width === null) {
            delete i.img_height;
            delete i.img_width;
          }
          return _.pick(i, FILEDS);
        })
        .first()
        .value());
    });

  });
};

/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getPhotosByMemberId = query => {

  // Get Data From DB
  var dbQuery = app.db_com.select(`${TABLE_GAL}.*`,
                                  `${TABLE_PIC}.file_name`,
                                  'missu_user.image_dimentions.width as img_width',
                                  'missu_user.image_dimentions.height as img_height') 
    

    .innerJoin(TABLE_PIC,`${TABLE_GAL}.thumbnail_photo_id`,`${TABLE_PIC}.id`)
    .innerJoin(TABLE_MA, `${TABLE_GAL}.id`, `${TABLE_MA}.gallery_id`)
    .where(`${TABLE_GAL}.app_published`, '=', 1)
    .orderBy(`${TABLE_GAL}.rank`, 'asc')
    .orderBy(`${TABLE_GAL}.published_date`, 'desc')
    .where(`${TABLE_GAL}.year`,'>=',2015)
    .where(`${TABLE_MA}.member_id`, query.member)
    .limit(query.limit)
    .table(TABLE_GAL)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE_PIC}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 4);
    });

  

  return dbQuery.then(rows => {

    return new Promise(res => {

      return res(
       _.chain(rows)
        .map(i => {
          i.image_url = `${app.config.aws.cf_url}photos/galleries/${i.file_name}`;
          if (i.img_height === null || i.img_width === null) {
            delete i.img_height;
            delete i.img_width;
          }
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};
