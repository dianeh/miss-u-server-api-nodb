
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */

const TABLE = 'app_images';
//const TABLE_MA = 'media_associations';
const FILEDS = [
  'id',
  'type',
  'image_url'
];


/**
 * @params {Number} query.year
 * @params {Number} query.limit
 * @params {Number} query.offset (Optional)
 *
 * @return Promise
 */
  exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .orderBy('id', 'desc')
    .table(TABLE);


  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .map(i => {
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};

/**
 * @params {Number} query.id
 *
 * @return Promise
 */
exports.getImageByType = query => {


  // Get Data From DB
  const dbQuery = app.db_user.select()

//   .where('year', '>=', query.year)

     //Only published
    .orderBy('id', 'desc')

    .where(`${TABLE}.type`, '=', query.type)
    .table(TABLE);

  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .map(i => {
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};
