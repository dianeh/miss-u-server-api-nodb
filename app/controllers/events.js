
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash');


/**
 * Constants
 * @private
 */
const TABLE = 'events_current';
const FILEDS = [
  'id',    //added for now
  'event_title',
  'image_url',
  'description',
  'date',
];


/**
 *   shows returns list of shows from shows table
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_user.select()
    .limit(query.limit)
    .table(TABLE);


  return dbQuery
  // Send only unique results - they all are, just keep like this for now
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('id')
        .map(i => {
          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};
