
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  striptags = require('striptags');


/**
 * Constants
 * @private
 */
const TABLE = 'news';
const FILEDS = [
  'id',
  'title',
  'link',
  'summary',
  'description',
  'description_raw',
  'year',
  'created',
  'app_published_date',
  'published_date',
  'modified',
  'image_url', // Added
  'type', // Added
  'rank', // ToDo
  'img_width',
  'img_height'
];


/**
 * @params {Number} query.year
 * @params {Number} query.limit
 * @params {Number} query.last_id (Optional)
 *
 * @return Promise
 */
exports.get = query => {

  // Get Data From DB
  const dbQuery = app.db_com.select(`missu.${TABLE}.*`,
                                    `missu_user.image_dimentions.height AS img_height`,
                                    `missu_user.image_dimentions.width AS img_width`)
    .where('year', '>=', query.year)
    .whereIn('app_published', [0, 1])
    .orderBy('rank', 'asc') 
    .orderBy('app_published_date', 'desc') 
    //.orderBy('created', 'id') // Client requested this during UAT testing app NEEDS change to use created  field instead of last_id, 
    .limit(query.limit)
    .table(TABLE)
    .leftJoin(`missu_user.image_dimentions`, function () {
      this.on(`missu_user.image_dimentions.item_id`, '=', `missu.${TABLE}.id`)
          .andOn(`missu_user.image_dimentions.table_id`, '=', 2);
    });

    // TODO: needs update from query.last_id to 
    // start using date ranges (see email on subject)
  // Build query
  query.offset &&
  dbQuery.offset(query.offset);


  return dbQuery
  // Send only unique results
  .then(rows => {

    return new Promise(res => {
      return res(
       _.chain(rows)
        .uniqBy('link')
        .map(i => {
          i.published_date = i.app_published_date;
          i.type = 'news';

          // i.image_url = `https://missuniversenews.files.wordpress.com/${i.year}/${i.id}/resize.jpeg`;
          i.image_url = i.image;

          // Strip html tags
          i.summary = striptags(i.summery).trim();
//          i.description_raw = i.description.trim();
          i.description_raw = '<h1>'+i.title.trim()+'</h1>'+i.description.trim();
          i.description = striptags(i.description).trim();

          if (i.img_width === null || i.img_height === null) {
            delete i.img_width;
            delete i.img_height;
          }

          return _.pick(i, FILEDS);
        })
        .value());
    });

  });
};
