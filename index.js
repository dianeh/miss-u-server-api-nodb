
/**
 * Module dependencies.
 * @private
 */
const helmet = require('helmet'),
  RequireAll = require('require-all'),
  mysql = require('./framework/mysql');

/**
 * Init the App
 */
global.app = null;
global.app = {};

/**
 * Setup Express Server & Framework
 */
app.server = require('express')();
app.server.disable('x-powered-by');
app.server.set('etag', false);
app.server.set('query parser', 'simple');
app.server.set('trust proxy', false);
app.server.disable('strict routing');
app.server.disable('view cache');
delete app.server.locals;
require('./framework/response_handler');

/**
 * Load Config
 */
app.config = require('./app/config')
  (app.server.get('env'));

/**
 * Logger
 */
app.server.use(require('morgan')('common'))

/**
 * Security Headers
 */
app.server.use(helmet());
app.server.use(helmet.noCache());

/**
 * Bodyparser
 */
app.server.use(require('body-parser').json({
  inflate: false,
  strict: true
}));

/**
 * HTTP Parameter Pollution Attacks
 */
app.server.use(require('hpp')({
  checkQuery: true,
  checkBody: false
}));

/**
 * Load Routes
 */
app.server.use('/status', (req, res) => res.success('system ok'));
require('./app/routes');

/**
 * 404 & Error Handler
 */
app.server.use(require('./framework/404_handler'));
app.server.use(require('./framework/error_handler'));

/**
 * Connect MySQL
 * Exports the app for testing
 */
module.exports = mysql.connect_com(app.config.mysql_com)
.then(db => {
  app.db_com = db;

  return mysql.connect_user(app.config.mysql_user);
})
.then(db => {
  /**
   * Save MySQL Instance
   */
  app.db_user = db;

  /**
   * Verify Tables & Build Schema
   */
  return mysql.buildSchema(app.config.mysql_user.tables);
})
.then(schema => {
  /**
   * Save schema
   */
  app.schema = schema;

  /**
   * Load Controllers
   */
  app.controllers = RequireAll({
    dirname: (__dirname + '/app/controllers'),
    filter:  /(.+)\.js$/, // Only JS files
    recursive: false
  });

  /**
   * Start the Server
   */
  app.server.listen(app.config.server.port, err => {
    console.info(`NodeJS Server Started @ port ${app.config.server.port}`);
  });
});
