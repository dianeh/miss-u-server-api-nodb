
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  util = require('util'),
  stackTrace = require('stack-trace');


/**
 * Constants
 */
const ERRORS = {
  400: 'Request parameters are invalid.',
  401: 'You are not authorized for this endpoint.',
  404: 'Route not found.',
  500: 'Some error occured, please try again.'
};


/**
 * Exception Handler
 * @private
 */
function exceptionHander(err) {

  // Gracefully handle exception
  errHandler(err);

  // Close DB connection
  app.db_com.destroy(() => {
    app.db_user.destroy(() => {
      process.exit(101);
    })
  });
};


/**
 * Error Handler
 * @private
 */
function errHandler(err) {

  const body = {
    stacktrace: {
      message: (err && err.message) || err,
      frames: stackTrace.parse(err)
    }
  };

  // Set stack-trace limit for certain cases
  if (process.env.TRACE_LIMIT) {
    body.stacktrace.frames.length = Number(process.env.TRACE_LIMIT)
  }

  // Log Error
  console.error(util.inspect(body, { depth: null }));
  return body;
};


/**
 * Uncaught exceptions
 * @private
 **/
process.removeAllListeners('uncaughtException');
process.on('uncaughtException', exceptionHander);

/**
 * Unhandled promise rejection
 * @private
 **/
process.on('unhandledRejection', exceptionHander);


/**
 * Expose Error Hander for Express
 */
module.exports = (err, req, res, next) => {

  const status = err.status || err.statusCode || 500;

  // Prep error body
  const body = status >= 500 ? errHandler(err) : {};
  body.message = ERRORS[status] || ERRORS[500];

  // Add error details
  if (err.details) {
    body.details = Array.isArray(err.details)
                    ? err.details
                    : [err.details];
  }

  if (app.config.env === 'production') {
    delete body.stacktrace;
  }

  return res.status(status).json({
    status: status,
    error: body
  });
};
