
/**
 * Module dependencies.
 * @private
 */
const Bcrypt = require('bcrypt'),
  jwt = require('jsonwebtoken');


/**
 * Constants
 */
const SALT_ROUNDS = 8;


/**
 * @params {String} password
 *
 * @public
 */
exports.hash = password => {

  return new Promise((resolve, reject) => {

    Bcrypt.hash(password, SALT_ROUNDS, (err, hashedPassword) => {
      if (err) return reject(err);
      return resolve(hashedPassword);
    });

  });
};


/**
 * @params {String} password
 * @params {String} hashedPassword
 *
 * @public
 */
exports.compare = (password, hashedPassword) => {

  return new Promise((resolve, reject) => {

    Bcrypt.compare(password, hashedPassword, (err, success) => {
      if (err) return reject(err);
      return resolve(success);
    });
  });
};


/**
 * @params {Object} data
 *
 * @public
 */
exports.signToken = data => {

  return new Promise((resolve, reject) => {
    return resolve(jwt.sign(data, app.config.jwtSecret));
  });
};


/**
 * @params {String} token
 *
 * @public
 */
exports.verifyToken = token => {

  return new Promise((resolve, reject) => {
    jwt.verify(token, app.config.jwtSecret, (err, data) => {
      if (data && data.id) return resolve(data);
      return resolve(false);
    });
  });
};
