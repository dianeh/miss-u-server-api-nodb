
/**
 * Build Success Response Handler for Express
 */
app.server.response.success = function(jsonData) {

  // Send JSON response
  return this.json({
    status: 200,
    success: jsonData
  });
};
