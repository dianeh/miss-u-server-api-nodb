
const aesjs = require('aes-js');
const key = aesjs.util.convertStringToBytes(app.config.crypto_key);

function encrypt(plaintext) {
  const textBytes = aesjs.util.convertStringToBytes(plaintext);
  const aesCtr = new aesjs.ModeOfOperation.ctr(key);
  const encryptedBytes = aesCtr.encrypt(textBytes);
  return encryptedBytes.toString('base64');
}

function decrypt(cryptictext) {
  const encryptedBytes = new Buffer(cryptictext, 'base64');
  const aesCtr = new aesjs.ModeOfOperation.ctr(key);
  const decryptedBytes = aesCtr.decrypt(encryptedBytes);
  return decryptedBytes.toString();
}


exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.decryptUser = dbUser => {
  const newUser = Object.assign({}, dbUser);
  if (newUser.email !== undefined && newUser.email !== null) {
    newUser.email = decrypt(newUser.email);
  }
  if (newUser.fb_id !== undefined && newUser.fb_id !== null) {
    newUser.fb_id = decrypt(newUser.fb_id);
  }
  if (newUser.tw_id !== undefined && newUser.tw_id !== null) {
    newUser.tw_id = decrypt(newUser.tw_id);
  }
  if (newUser.fb_token !== undefined && newUser.fb_token !== null) {
    newUser.fb_token = decrypt(newUser.fb_token);
  }
  if (newUser.tw_token !== undefined && newUser.tw_token !== null) {
    newUser.tw_token = decrypt(newUser.tw_token);
  }
  return newUser;
};
exports.encryptUser = dbUser => {
  const newUser = Object.assign({}, dbUser);
  if (newUser.email !== undefined && newUser.email !== null) {
    newUser.email = encrypt(newUser.email);
  }
  if (newUser.fb_id !== undefined && newUser.fb_id !== null) {
    newUser.fb_id = encrypt(newUser.fb_id);
  }
  if (newUser.tw_id !== undefined && newUser.tw_id !== null) {
    newUser.tw_id = encrypt(newUser.tw_id);
  }
  if (newUser.fb_token !== undefined && newUser.fb_token !== null) {
    newUser.fb_token = encrypt(newUser.fb_token);
  }
  if (newUser.tw_token !== undefined && newUser.tw_token !== null) {
    newUser.tw_token = encrypt(newUser.tw_token);
  }
  return newUser;
}