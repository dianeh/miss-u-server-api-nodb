
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  joiHelpers = require('joi-helpers');


/***
* DB Object (only one instance)
*/
var DB_USER, DB_COM, SCHEMAS = {};


/**
 * @params {String} opts.host (Default = '127.0.0.1')
 * @params {String} opts.user (Default = 'user')
 * @params {String} opts.password (Default = 'password')
 * @params {String} opts.database (Default = 'db_app')
 * @params {Boolean} opts.debug (Optional, Default = false)
 * @params {Object} opts.pool
 *
 * @return {Promise}
 */
exports.connect_user = opts => {

  return new Promise((resolve, reject) => {

    // Check if DB is already instantiated:
    if (DB_USER) {
      return resolve(DB_USER);
    }

    opts = Object.assign({
      debug: false,
      client: 'mysql',
      host: '127.0.0.1',
      user: 'user',
      password: 'password',
      database: 'db_app',
      pool: undefined,
      charset: 'UTF8MB4_UNICODE_CI'
    }, opts);


    // Init Knex
    const knex = require('knex')({
      client: opts.client,
      connection: opts,
      pool: opts.pool
    });

    DB_USER = knex;
    return resolve(DB_USER);
  });
};

exports.connect_com = opts => {

  return new Promise((resolve, reject) => {

    // Check if DB is already instantiated:
    if (DB_COM) {
      return resolve(DB_COM);
    }

    opts = Object.assign({
      debug: false,
      client: 'mysql',
      host: '127.0.0.1',
      user: 'user',
      password: 'password',
      database: 'db_app',
      pool: undefined
    }, opts);


    // Init Knex
    const knex = require('knex')({
      client: opts.client,
      connection: opts,
      pool: opts.pool
    });

    DB_COM = knex;
    return resolve(DB_COM);
  });
};

/**
 * @params {Array} tables
 *
 * @return {Promise}
 * @public
 */
exports.buildSchema = tables => {

  return new Promise((resolve, reject) => {

    const P = _.reduce(tables, (ret, t) => {
      // Validate if schema already exists
      if (! SCHEMAS[t])
        ret.push(app.db_user.schema.hasTable(t));

      return ret;
    }, []);


    Promise.all(P)

    .then(exists => {

      _.forEach(exists, (exist, i) => {
        console.info(`MySQL "${tables[i]}" exists: ${exist}`);
        SCHEMAS[tables[i]] = true;
      });

      return Promise.all(_.map(tables, t => {
        return app.db_user(t).columnInfo();
      }));
    })

    .then(schemas => {

      return resolve(
        _.zipObject(
          tables,
          _.map(schemas, schema => {
            // Compile Joi Schema from Sql
            return joiHelpers.sqlSchemaCompile(schema);
          })
      ));
    })

    .catch(err => {
      return reject(err);
    });

  });
};
