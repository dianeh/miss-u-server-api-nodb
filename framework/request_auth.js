
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  crypto = require('./crypto');


/**
 * Constants
 * @private
 */
const AUTH_HEADER = 'x-session-token';


/**
 * @params {Array} Roles = ['user','guest']
 */
module.exports = Roles => {

  // Build Roles Object
  Roles = _.zipObject(Roles, _.fill(Array(Roles.length), true));
  const RolesStr = _.keys(Roles).join(', ');


  /**
   * Expose Auth Hander for Express
   */
  return(req, res, next) => {

    const token = req.get(AUTH_HEADER);
    req.user = req.user || {};


    function authErr() {
      // Prep error
      const err = new Error();
      err.status = 401;
      err.details = `Requires [${RolesStr}] roles.`;
      return next(err);
    }

    if (token) {

      console.info(`x-session-token: ${token}`);

      crypto.verifyToken(token)
      .then(user => {

        // Validate role
        if (typeof user.id !== 'undefined' &&
            Roles[user.role]) {

          req.user = user;
          return next();
        }

        return authErr();
      });
    }

    else {
      return authErr();
    }

  };
};
