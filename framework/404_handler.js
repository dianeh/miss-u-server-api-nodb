
/**
 * Expose 404 Hander for Express
 */
module.exports = (req, res, next) => {

  err = new Error('Route not found.');
  err.status = 404;
  return next(err);
};
