
/**
 * Module dependencies.
 * @private
 */
const _ = require('lodash'),
  joiHelpers = require('joi-helpers');


/**
 * @params {Object} schema
 * @params {String} type (Optional)
 *
 * @public
 */
module.exports = (schema, type) => {

  // Create schema
  schema = joiHelpers.compile(schema);


  // Return Express Middleware
  return (req, res, next) => {

    // Only define type once
    type = type || (req.method === 'POST' ? 'body' : 'query');


    // Validate request
    const validate = joiHelpers.validate(schema, req[type]);

    if (validate.error) {

      // Prep error
      const err = new Error();
      err.status = 400;

      err.details = _.map(validate.error, 'message');
      return next(err);
    }

    // Sanitized object
    req[type] = validate.value;
    return next();
  }
};
